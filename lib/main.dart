import 'package:flutter/material.dart';
import 'gui/views/Home.dart';
import 'gui/views/Map.dart';
import 'gui/views/Stats.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/Home',
      routes: {
        '/Home': (_)=>MyHomePage(),
        '/Map': (_)=>MapScreen(),
        '/Stats': (_)=>StatsScreen()
      }
    );
  }
}


