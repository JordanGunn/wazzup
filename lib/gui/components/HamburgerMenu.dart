import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HamburgerMenu extends StatefulWidget {
  const HamburgerMenu({Key key}) : super(key: key);

  @override
  State<HamburgerMenu> createState() => _HamburgerMenuState();
}

class _HamburgerMenuState extends State<HamburgerMenu> {
  List<String> menu = ['Home', 'Map', 'Stats'];
  String menuValue = 'Home';

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      dropdownColor: Color.fromARGB(255, 22, 121, 97),
      // value: menuValue,
      icon: const Icon(
        Icons.menu,
        color: Color.fromARGB(255, 22, 121, 97),
      ),
      iconSize: 50,
      elevation: 16,
      style: const TextStyle(
        color: Colors.white,
      ),
      borderRadius: BorderRadius.circular(5),
      onChanged: (String newValue) {
        setState(() {
          menuValue = newValue;
          Navigator.pushNamed(context, '/$menuValue');
        });
      },
      items: menu.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(
                fontFamily: "Montserrat",
                fontSize: 18,
                  fontWeight: FontWeight.bold
              ),
              ),
        );
      }).toList(),
    );
  }
}
