
import 'package:flutter/cupertino.dart';


class GreenBackgroundWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
        image: DecorationImage(
        image: AssetImage("assets/background.png"), // <-- BACKGROUND IMAGE
        fit: BoxFit.cover,
        )
      )
    );
  }
}