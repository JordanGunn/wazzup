import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  // FONT: montserrat
  // FONT SIZE: bold
  // FONT WEIGHT: BOLD

  @override
  Widget build(BuildContext context) {
    return
      Card(
        color: Colors.transparent,
        elevation: 0.0,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              child: Icon(
                Icons.masks,
                color: Color.fromARGB(255, 37, 150, 123),
                size: 150,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 180),
              child: Text(
                'wazzup',
                  style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.bold,
                  fontSize: 32,
                ),
              ),
            ),
          ],
        ),
      );
  }
}