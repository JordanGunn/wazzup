import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wuzzup/gui/components/HamburgerMenu.dart';

class MapScreen extends StatefulWidget {
  MapScreen({Key key}) : super(key: key);
  @override
  State<MapScreen> createState() => MapSampleState();
}

class MapSampleState extends State<MapScreen> {
  Completer<GoogleMapController> _controller = Completer();
  Future<Data> _data;
  List<Marker> markers = <Marker>[];
  Map<String, Color> statusColors = {
    "Good": Color.fromARGB(255, 0, 153, 102),
    "Moderate": Color.fromARGB(255, 255, 222, 51),
    "Bad": Color.fromARGB(255, 255, 153, 51),
    "Severe": Color.fromARGB(255, 204, 0, 51)
  };

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(49, -123.0),
    zoom: 10.0,
  );

  List<Marker> getMarkers(Data dat) {
    Map data = dat.data;
    dat.avg = 0;
    for (int i = 0; i < data["data"].length; i++) {
      markers.add(Marker(
          markerId: MarkerId(data["data"][i]["uid"].toString()),
          position: LatLng(data["data"][i]["lat"], data["data"][i]["lon"]),
          infoWindow: InfoWindow(
              title: data["data"][i]["station"]["name"],
              snippet: "Air Quality:\t${data["data"][i]['aqi']}")));
      dat.avg += (isNumeric(data["data"][i]['aqi']))
          ? double.parse(data["data"][i]['aqi'])
          : 0;
    }
    dat.avg /= data["data"].length;
    return markers;
  }

  @override
  Widget build(BuildContext context) {
    // get the markers

    return new Scaffold(
      appBar: AppBar(
          backgroundColor: Color.fromARGB(255, 87, 192, 167),
          elevation: 0.0,
          actions: [
            Container(
                margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
                child: HamburgerMenu()),
            Container(
              margin: EdgeInsets.fromLTRB(5, 0, 30, 0),
              child: IconButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/Home');
                },
                icon: Icon(Icons.home, size: 50),
              ),
            ),
          ]),
      body: FutureBuilder(
          future: _data,
          builder: (context, snapshot) {
            return snapshot.hasData
                ? Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                    Expanded(
                      flex:1,
                      child: GoogleMap(
                        mapType: MapType.hybrid,
                        markers: Set<Marker>.of(getMarkers(snapshot.data)),
                        initialCameraPosition: _kGooglePlex,
                        onMapCreated: (GoogleMapController controller) {
                          _controller.complete(controller);
                        },
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(40),
                          bottomRight: Radius.circular(40)
                        ),
                          color: Colors.white,
                          border: Border.all(
                              color: statusColors[
                                  severityController(snapshot.data.avg)],
                              width: 8,
                          )),
                      width: double.infinity,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            child: Text(severityController(snapshot.data.avg),
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 40,
                                    color: statusColors[
                                        severityController(snapshot.data.avg)],
                                    fontWeight: FontWeight.bold)),
                          ),
                          Container(
                            child: Text(
                                "Air Quality: ${snapshot.data.avg.toStringAsFixed(2)}",
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 28,
                                    color: statusColors[
                                        severityController(snapshot.data.avg)],
                                    fontWeight: FontWeight.bold)),
                          ),
                        ],
                      ),
                    )
                  ])
                : Center(
                    child: CircularProgressIndicator(),
                  );
          }),
    );
  }

  Future<Data> getAirQuality() async {
    String endPoint = END_POINT();
    Response response = await get(Uri.parse(endPoint));
    return Data(response: response, data: jsonDecode(response.body));
  }

  String END_POINT() {
    String token = "a92c3175b58962045c6bb5a0b686e3fdd6991531";
    String latLng = "48.30,-139.06,60.00,-114.03";
    String domain = "http://api.waqi.info/map/bounds/";
    String endPoint;
    endPoint = (domain + "?token=" + token + "&latlng=" + latLng);
    return endPoint;
  }

  String severityController(double value) {
    if (value <= 50) {
      return "Good";
    }

    if (value > 50 && value <= 92) {
      return "Moderate";
    }

    if (value > 92 && value <= 106) {
      return "Bad";
    }

    if (value > 106) {
      return "Severe";
    }
  }

  @override
  void initState() {
    super.initState();
    _data = getAirQuality();
  }
}

bool isNumeric(String s) {
  if (s == null) {
    return false;
  }
  return double.tryParse(s) != null;
}

class Data {
  Response response;
  Map data;
  double avg;

  Data({this.response, this.data});
}
