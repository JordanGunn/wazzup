import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wuzzup/gui/components/Background.dart';
import 'package:wuzzup/gui/components/HamburgerMenu.dart';
import 'package:wuzzup/gui/components/Logo.dart';

class MyHomePage extends StatefulWidget {
  static const String route = "/Home";
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
            backgroundColor: Color.fromARGB(255, 87, 192, 167),
            elevation: 0.0,
            actions: [
              Container(
                  margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
                  child: HamburgerMenu()),
              Container(
                margin: EdgeInsets.fromLTRB(5, 0, 30, 0),
                child: IconButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/Home');
                  },
                  icon: Icon(Icons.home, size: 50),
                ),
              ),
            ]),
        body: Stack(
          alignment: AlignmentDirectional.center,
          children: <Widget>[
            GreenBackgroundWidget(),
            Align(alignment: Alignment.center, child: Logo()),
            Container(
              color: Colors.transparent,
              margin: EdgeInsets.fromLTRB(0, 550, 0, 0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: Stack(
                  children: <Widget>[
                    Positioned.fill(
                      child: Container(
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: Color(0xFF0A7B6E), width: 5.0),
                          color: Colors.white,
                        ),
                      ),
                    ),
                    TextButton(
                      style: TextButton.styleFrom(
                        padding: const EdgeInsets.all(16.0),
                        primary: Color(0xFF0DA18B),
                        textStyle: const TextStyle(
                            fontSize: 30, fontWeight: FontWeight.w500),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(
                          context,
                          '/Map',
                        );
                      },
                      child: const Text('LOCAL AIR QUALITY'),
                    ),
                  ],
                ),
              ),
            ),
          ],
        )
        // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
