import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';

import 'package:wuzzup/gui/components/HamburgerMenu.dart';


class StatsScreen extends StatefulWidget {

  @override
  State<StatsScreen> createState() => _StatsScreenState();
}

class _StatsScreenState extends State<StatsScreen> {
  Future<Data> _data;
  List<String> days;

  Map<String, Color> statusColors = {
    "Good": Color.fromARGB(255, 0, 153, 102),
    "Moderate": Color.fromARGB(255, 255, 222, 51),
    "Bad": Color.fromARGB(255, 255, 153, 51),
    "Severe": Color.fromARGB(255, 204, 0, 51)
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Color.fromARGB(255, 87, 192, 167),
            elevation: 0.0,
            actions: [
              Container(
                  margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
                  child: HamburgerMenu()),
              Container(
                margin: EdgeInsets.fromLTRB(5, 0, 30, 0),
                child: IconButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/Home');
                  },
                  icon: Icon(Icons.home, size: 50),
                ),
              ),
            ]),
        body:
    FutureBuilder(
          future: _data,
          builder: (context, snapshot) {
            return snapshot.hasData
                ?
            Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.white,
                      Color.fromARGB(255, 22, 121, 97)
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 20,
                    right: 20,
                  ),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        _maskIcon(),
                        _aqi(snapshot.data.mydata["data"]["aqi"].toString()),
                        _location(),
                        _weekly(),
                        _weeklyPrediction(snapshot.data.mydata),
                        _today(),
                        _pollutants(
                            snapshot.data.mydata
                        ),
                        _date(snapshot.data.mydata["data"]["time"]["s"]),
                      ]
                  ),
                )
            )
            : Center(
              child: CircularProgressIndicator()
            );
          }
        )
    );
  }

  _pollutants(Map list) {
    List<String> k_values = ["Carbon Monoxide", "Hydrogen", "Nitrogen Dioxide", "Ozone", "Sulphur Dioxide"];

    List<String> values = [
      list["data"]["iaqi"]["co"]["v"].toString(),
      list["data"]["iaqi"]["h"]["v"].toString(),
      list["data"]["iaqi"]["no2"]["v"].toString(),
      list["data"]["iaqi"]["o3"]["v"].toString(),
      list["data"]["iaqi"]["so2"]["v"].toString(),
    ];

    return Expanded(
        child: Container(
            height:100,
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(color: Colors.white),
                bottom: BorderSide(color: Colors.white),
              ),
            ),
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: values.length,
                itemBuilder: (context, index){
                  return Opacity(
                    alwaysIncludeSemantics: false,
                    opacity: 0.7,
                    child: Container(
                        width: 50,
                        child: Card(
                            child: Center(
                                child: Text('${k_values[index]}: ${values[index]}',
                                  style: TextStyle(
                                      fontFamily: "Montserrat",
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold
                                  ),
                                )
                            )
                        )
                    ),
                  );
                })
        )
    );
  }

  _weeklyPrediction(Map list) {
    List<int> dailyAverage = [];
    for (int i = 0; i < list["data"]["forecast"]["daily"]["pm25"].length; i++) {
      dailyAverage.add(
          list["data"]["forecast"]["daily"]["pm25"][i]["avg"]
      );
    }

    List<String> weekday = [
      "Mon",
      "Tue",
      "Wed",
      "Thur",
      "Fri",
      "Sat",
      "Sun"
    ];

    return Container(
        height:100,
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(7)),
          border: Border(
            right: BorderSide(color: Colors.white),
            top: BorderSide(color: Colors.white),
            bottom: BorderSide(color: Colors.white),
            left: BorderSide(color: Colors.white),
          ),
        ),
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: dailyAverage.length,
            itemBuilder: (context, index){
              return Container(
                  color: Color.fromRGBO(255, 255, 255, 0.4),
                    width: 50,
                    child: Card(
                        color: statusColors[
                        severityController(dailyAverage[index])],
                        child: Center(
                            child: Text("${weekday[index%7]}\n  ${dailyAverage[index]}",
                              style: TextStyle(
                                  fontFamily: "Montserrat",
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold
                              ),
                            )
                        )
                    )
              );
            })
    );
  }
  _date(String date) {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Last Updated: '),
          SizedBox(
              width: 10
          ),
          Text(
              date
          )
        ],
      ),
    );
  }

  _location() {
    return Row(
      children: const [
        Icon(Icons.place),
        SizedBox(
            width: 10
        ),
        Text(
            'Vancouver, BC',
          style: TextStyle(
              fontFamily: "Montserrat",
              fontSize: 12,
              fontWeight: FontWeight.bold
          ),
        )
      ],
    );
  }

  _today() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: Center(
          child: Text(
            "TODAY's POLLUTANT METRICS",
            style: TextStyle(
                fontFamily: "Montserrat",
                fontSize: 15,
                fontWeight: FontWeight.bold
            ),
          )
      ),
    );
  }

  _weekly() {
    return
      Container(
        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
        child: Center(
          child: Text(
            "WEEKLY AIR QUALITY INDEX",
            style: TextStyle(
                fontFamily: "Montserrat",
                fontSize: 15,
                fontWeight: FontWeight.bold
            ),
          )
    ),
      );
  }

  _maskIcon() {
    return const Padding(
      padding: EdgeInsets.fromLTRB(28.0, 56.0, 28.0, 10.0),
      child: Icon(
          Icons.masks,
          size: 150,
          color: Color.fromARGB(255, 22, 121, 97)
      ),
    );
  }

  _aqi(String aqi) {
    return Text("Air Quality Index: ${aqi}",
        style: TextStyle(
          fontFamily: "Montserrat",
            fontSize: 30,
            fontWeight: FontWeight.bold
        )
    );
  }

  @override
  void initState() {
    super.initState();
    _data = getAirQuality();
  }

  Future<Data> getAirQuality() async {
    String endPoint = END_POINT();
    Response response = await get(Uri.parse(endPoint));
    return Data(response: response, mydata: jsonDecode(response.body));
  }

  String END_POINT() {
    String token = "a92c3175b58962045c6bb5a0b686e3fdd6991531";
    String domain = "http://api.waqi.info/feed/Vancouver/";
    String endPoint;
    endPoint = (domain + "?token=" + token);
    return endPoint;
  }

  String severityController(int value) {
    if (value <= 5) {
      return "Good";
    }

    if (value > 5 && value <= 10) {
      return "Moderate";
    }

    if (value > 15 && value <= 25) {
      return "Bad";
    }

    if (value > 25) {
      return "Severe";
    }
  }
}

class Data {
  Response response;
  Map mydata;

  Data({this.response, this.mydata});
}


















//
// class MapScreen extends StatefulWidget {
//   MapScreen({Key key}) : super(key: key);
//   @override
//   State<MapScreen> createState() => MapSampleState();
// }
//
// class MapSampleState extends State<MapScreen> {
//   Future<Data> _data;
//   Map<String, Color> statusColors = {
//     "Good": Color.fromARGB(255, 0, 153, 102),
//     "Moderate": Color.fromARGB(255, 255, 222, 51),
//     "Bad": Color.fromARGB(255, 255, 153, 51),
//     "Severe": Color.fromARGB(255, 204, 0, 51)
//   };
//
//
//   // List<Marker> getMarkers(Data dat) {
//   //   Map data = dat.data;
//   //   dat.avg = 0;
//   //   for (int i = 0; i < data["data"].length; i++) {
//   //     markers.add(Marker(
//   //         markerId: MarkerId(data["data"][i]["uid"].toString()),
//   //         position: LatLng(data["data"][i]["lat"], data["data"][i]["lon"]),
//   //         infoWindow: InfoWindow(
//   //             title: data["data"][i]["station"]["name"],
//   //             snippet: "Air Quality:\t${data["data"][i]['aqi']}")));
//   //     dat.avg += (isNumeric(data["data"][i]['aqi']))
//   //         ? double.parse(data["data"][i]['aqi'])
//   //         : 0;
//   //   }
//   //   dat.avg /= data["data"].length;
//   //   return markers;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     // get the markers
//
//     return new Scaffold(
//       appBar: AppBar(
//           backgroundColor: Color.fromARGB(255, 87, 192, 167),
//           elevation: 0.0,
//           actions: [
//             Container(
//                 margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
//                 child: HamburgerMenu()),
//             Container(
//               margin: EdgeInsets.fromLTRB(5, 0, 30, 0),
//               child: IconButton(
//                 onPressed: () {
//                   Navigator.pushNamed(context, '/Home');
//                 },
//                 icon: Icon(Icons.home, size: 50),
//               ),
//             ),
//           ]),
//       body: FutureBuilder(
//           future: _data,
//           builder: (context, snapshot) {
//             return snapshot.hasData
//                 ? Column(
//               // crossAxisAlignment: CrossAxisAlignment.stretch,
//
//                 children: <Widget>[
//                   Container(
//                     decoration: BoxDecoration(
//                         color: Colors.white,
//                         border: Border.all(
//                             color: statusColors[
//                             severityController(snapshot.data.avg)],
//                             width: 5)),
//                     width: double.infinity,
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [
//                         Container(
//                           child: Text(severityController(snapshot.data.avg),
//                               style: TextStyle(
//                                   fontFamily: "Montserrat",
//                                   fontSize: 40,
//                                   color: statusColors[
//                                   severityController(snapshot.data.avg)],
//                                   fontWeight: FontWeight.bold)),
//                         ),
//                         Container(
//                           child: Text(
//                               "Air Quality: ${snapshot.data.avg.toStringAsFixed(2)}",
//                               style: TextStyle(
//                                   fontFamily: "Montserrat",
//                                   fontSize: 28,
//                                   color: statusColors[
//                                   severityController(snapshot.data.avg)],
//                                   fontWeight: FontWeight.bold)),
//                         ),
//                       ],
//                     ),
//                   )
//                 ])
//                 : Center(
//               child: CircularProgressIndicator(),
//             );
//           }),
//     );
//   }
//
//
//
//
//
//
//
//
// }




